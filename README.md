# README #

##Nombre del proyecto y de todos los sub-módulos y librerías##
El nombre del proyecto es colaAmigos. 
El cual contiene las clases : 

* Persona.java

* ColaAmigos.java

Ambas almacenadas en src/main/java/colaAmigos

Por otro lado estan los tests de estas clases: 

* TestPersona.java

* TestColaAmigos.java

* Y para completar, un test en aislamiento para colaAmigos: MockTestColaAmigos.java

Estos últimos se encuentran en src/test/java/colaAmigos

Las librerías utilizadas están en :file_folder: lib y son:

* easymock-3.4

* hamcrest-core-1.3

* junit-4.11

* objenesis-2.2

Cada una de estas librerias son necesarias para alguna de las partes del proyecto (easymock para tests en aislamiento, junit para la implementacion de los tests en general)

##Descripción del proyecto##
El proyecto requiere de Java 7 para ejecutarse sin problemas de compatibilidad. 

Este proyecto Maven representa una cola con un orden especial en la cual cada persona que entra puede reservar sitio para sus amigos. Las personas que llegan pueden colarse si es que tienen un amigo ya en la cola (con sitio reservado disponible) o ponerse al final reservando (o no) sitio para sus amigos.

Como ya se ha aclarado, alguien solo puede colarse si tiene un amigo en la cola, respecto a esto, se considera que si esa persona es amiga tuya, tu eres amigo de él. Por lo tanto, las relaciones: amistad y conocido se han considerado recíprocas ambas.
 
##Cómo se usa##
Ejecutando el archivo build.xml obtenemos distintas carpetas

* :file_folder: target

*  :file_folder: out

* :file_folder: resultados

* :file_folder: doc

Target contiene los .class del proyecto. Out contiene el resultado de la ejecucion de los tests en archivos xml. Resultados contiene los analisis de cobertura en archivos xml. Doc contiene el javaDoc generado en archivos xml, para verla completa solo tenemos que abrir el archivo index.xml.

build.xml va a automatizar todas las tareas del proyecto, desde limpiar, ejecutar y compilar hasta la ejecución del los tests TDD o en aislamiento, pasando por la generacion de la documentación. También va a obtener un informe de cobertura. 

##Autores##
Rebeca Rodríguez Feria