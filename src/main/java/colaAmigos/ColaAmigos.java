package colaAmigos;

import java.util.ArrayList;

public class ColaAmigos {
	protected ArrayList<Persona> cola = new ArrayList<Persona>();
	
	/**
	 * Constructor de la clase
	 */
	public ColaAmigos(){
	}
	
	/**
	 * Metodo para aniadir a la cola y reservar sitio
	 */
	public void pedirLaVez(Persona p, int reserva){
		if(p.equals(null))
			throw new NullPointerException();
		else{
			if(reserva>10 || reserva<0)
				throw new AssertionError();
			cola.add(p);
			p.setReserva(reserva);
		}
	}
	
	/**
	 * Metodo para aniadir a la cola colandose. Atributo flag
	 */
	public void colarse(Persona p){
		if(p.equals(null))
			throw new NullPointerException();
		for(int i=0; i <= cola.size(); i++){
			if(i==cola.size())
				throw new AssertionError();
			else{
				if (p.esAmigo(cola.get(i)) && faltanPorColar(cola.get(i))>0){
					cola.add(i, p);
					break;
				}
			}
		}
	}
	
	/**
	 * Metodo para comprobar para cuantos amigos ha
	 * reservado sitio una persona
	 * @return
	 */
	public int consultaReserva(Persona p){
		if(p.equals(null))
			throw new NullPointerException();
		else{
			if(!cola.contains(p))
				throw new AssertionError();
			return p.getReserva();
		}
	}
	
	/**
	 * Metodo para saber cuantas personas puede aun
	 * colar otra Persona
	 * @return
	 */
	public int faltanPorColar(Persona p){
		System.out.println("Reserva:"+p.getReserva());
		System.out.println("AmigosEnCola:"+consultaAmigosEnCola(p));
		return (p.getReserva() - consultaAmigosEnCola(p));
	}
	
	/**
	 * Metodo que devuelve el numero de personas que son
	 * amigos colados de otro de los que estan delante suyo
	 * @return amigos numero de amigos en la cola (colados)
	 */
	public int consultaAmigosEnCola(Persona p){
		int i, j, amigos;
		boolean flag = true;
		if(p.equals(null))
			throw new NullPointerException();
		else{
			if(!cola.contains(p))
				throw new AssertionError();
			else{
				i= p.getReserva();
				j=cola.indexOf(p)-1;
				amigos = 0;
				while(i>0 && j>=0 && flag){
					if(p.amigos.contains(cola.get(j))){
						amigos++; i--; j--;
					}else
						flag = false;
				}
				return amigos;
			}
		}
	}
	
	/**
	 * Metodo que devuelve la persona a la que le toca el turno
	 * @return
	 */
	public Persona siguiente(){
		return cola.get(0);
	}
	
	/**
	 * Metodo para eliminar a alguien de la cola
	 */
	public void atender(){
		if(cola.size()==0)
			throw new IndexOutOfBoundsException();
		cola.remove(siguiente());
	}
	
	public ArrayList<Persona> getCola(){
		return cola;
	}
	
	
	
}
