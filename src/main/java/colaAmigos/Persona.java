package colaAmigos;

import java.util.ArrayList;

public class Persona {
	protected ArrayList<Persona> conocidos = new ArrayList<Persona>();
	protected ArrayList<Persona> amigos = new ArrayList<Persona>();
	protected String nombre;
	protected int reserva;
	
	/**
	 * Constructor de la clase
	 */
	public Persona(String nombre){
		this.nombre = nombre;
	}
	/**
	 * Metodo para comprobar si una persona es amiga
	 * de otra
	 * @return
	 */
	public boolean esAmigo(Persona p){
		if(p.equals(null))
			throw new NullPointerException();
		return amigos.contains(p);
	}
	/**
	 * Metodo para dar a conocer una persona a otra
	 * @param p
	 */
	public void conocer(Persona p){
		if(p.equals(null))
			throw new NullPointerException();
		else{
			if(conocidos.contains(p))
				throw new AssertionError();
			conocidos.add(p);
			p.getConocidos().add(this);
		}
	}
	
	/**
	 * Metodo para trabar amistad entre dos personas
	 * @param p
	 */
	public void serAmigo(Persona p){
		if(p.equals(null))
			throw new NullPointerException();
		else{
			if(amigos.contains(p))
				throw new AssertionError();
			else{
				if(!conocidos.contains(p))
					throw new AssertionError();
				conocidos.remove(p);
				p.getConocidos().remove(this);
				amigos.add(p);
				p.getAmigos().add(this);
			}	
		}
	}
	
	/**
	 * Metodo para dejar de ser amigo de otra persona
	 * (esta pasa a ser un conocido)
	 * @param p
	 */
	public void dejarSerAmigo(Persona p){
		if(p.equals(null))
			throw new NullPointerException();
		else{
			if(!amigos.contains(p))
				throw new AssertionError();
			else{
				amigos.remove(p);
				p.getAmigos().remove(this);
				conocidos.add(p);
				p.getConocidos().add(this);
			}
		}
	}

	/**
	 * Getter conocidos. Obten todos los conocidos de una persona
	 * @return conocidos
	 */
	public ArrayList<Persona> getConocidos() {
		return conocidos;
	}

	/**
	 * Getter amigos. Obten todos los amigos de una persona
	 * @return amigos
	 */
	public ArrayList<Persona> getAmigos() {
		return amigos;
	}

	/**
	 * Getter nombre. Obten el nombre de una persona
	 * @return nombre
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Getter reserva. Obten el numero de sitios que ha reservado
	 * de una persona
	 * @return nombre
	 */
	public int getReserva() {
		return reserva;
	}
	
	/**
	 * Setter reserva. Modifica el atributo reserva
	 * @return nombre
	 */
	public void setReserva(int reserva) {
		this.reserva = reserva;
	}
	

}
