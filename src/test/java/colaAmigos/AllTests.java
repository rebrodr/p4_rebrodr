package colaAmigos;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	TestPersona.class,
	TestColaAmigos.class
})

public class AllTests {
}
