package colaAmigos;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestColaAmigos {
	protected ColaAmigos c;
	protected Persona p1;
	protected Persona p2;

	@Before
	public void setUp() {
		c = new ColaAmigos();
		p1 = new Persona("luis");
		p2 = new Persona("ana");
	}
	
	@After
	public void tearDown() {
		c = null;
		p1 = null;
		p2 = null;
	}
	
	/**
	 * Test para comprobar que se ha creado bien una
	 * cola, de ser asi deberia tener un array vacio (tamaño 0)
	 */
	@Test
	public void testEsValida(){
		assertEquals(c.getCola().size(), 0);
	}
	
	/**
	 * Test para comprobar que se pide la vez
	 * correctamente, al acabar se debe tener en la cola a la
	 * persona que pide vez
	 */
	@Test
	public void testPedirLaVez(){
		int reserva = 0;
		c.pedirLaVez(p1, reserva);
		assertTrue(c.getCola().contains(p1));
	}
	
	/**
	 * Test para impedir que una persona
	 * intente entrar en la cola ya estando (a traves del
	 * metodo pedir la vez)
	 */
	@Test (expected=AssertionError.class)
	public void testPedirLaVezRepetida(){
		int reserva =1;
		c.pedirLaVez(p1, reserva);
		c.pedirLaVez(p1, reserva);
	}
	
	/**
	 * Test para impedir que null pueda 
	 * pedir la vez 
	 */
	@Test (expected = NullPointerException.class)
	public void testPedirLaVezNoNull(){
		int reserva = 0;
		Persona p = null;
		c.pedirLaVez(p, reserva);
	}
	
	/**
	 * Test para impedir que una persona reserve
	 * sitio para un numero negativo de personas
	 */
	@Test  (expected = AssertionError.class)
	public void testPedirLaVezNegativa(){
		int reserva = -1;
		c.pedirLaVez(p1, reserva);
	}
	
	/**
	 * Test para limitar el numero de personas
	 * para las que se puede reservar a 10
	 */
	@Test (expected=AssertionError.class)
	public void testPedirLaVezMaxLim(){
		int reserva = 11;
		c.pedirLaVez(p1, reserva);
	}
	
	/**
	 * Test para comprobar que se realiza
	 * correctamente el metodo colarse, de ser asi la persona que se 
	 * cuela debe estar en la cola y las personas que le quedan por colar
	 * al que reservo deben decrementarse en 1
	 */
	@Test
	public void testColarse(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		c.pedirLaVez(p1, 1);
		c.colarse(p2);
		assertTrue(p2.esAmigo(p1));
		assertEquals(c.faltanPorColar(p1),p1.getReserva()-c.consultaAmigosEnCola(p1));
	}
	
	/**
	 * Test para evitar que alguien que ya
	 * esta en la cola se cuele de nuevo
	 */
	@Test (expected=AssertionError.class)
	public void testColarseRepetido(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		c.pedirLaVez(p1, 1);
		c.colarse(p2);
		c.colarse(p2);
	}
	/**
	 * Test para evitar que alguien se cuele
	 * sin tener ningun amigo con sitios reservados
	 */
	@Test (expected=AssertionError.class)
	public void testNoColarseSinSitio(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		c.pedirLaVez(p1, 0);
		c.colarse(p2);
		assertNotEquals(c.faltanPorColar(p1),0);
	}
	
	/**
	 * Test epara evitar que se pase un null como 
	 * parametro al metodo colarse
	 */
	@Test (expected=NullPointerException.class)
	public void testNoColarseNull(){
		c.pedirLaVez(p1, 1);
		c.colarse(null);
	}
	
	/**
	 * Test para evitar que se cuele una
	 * persona sin amigos en la cola
	 */
	@Test (expected=AssertionError.class)
	public void testNoColarseSinAmigo(){
		p1.conocer(p2);
		c.pedirLaVez(p1, 1);
		c.colarse(p2);
		assertTrue(p2.esAmigo(p1));
	}
	
	/**
	 * Test para comprobar si consultar la reserva de
	 * una persona se realiza correctamente
	 */
	@Test
	public void testConsultaReserva(){
		int reserva = 2;
		c.pedirLaVez(p1, reserva);
		assertEquals(c.consultaReserva(p1), p1.reserva);
	}
	
	/**
	 * Test para evitar que se pase un 
	 * null como parametro al metodo consultarReserva()
	 */
	@Test (expected = NullPointerException.class)
	public void testNoConsultaReservaNull(){
		int reserva = 2;
		c.pedirLaVez(p1, reserva);
		c.consultaReserva(null);
	}
	
	/**
	 * Test para controlar que se consulte la
	 * la reserva de alguien que no esta en la cola
	 */
	@Test (expected = AssertionError.class)
	public void testNoConsultaReservaNoCola(){
		c.consultaReserva(p1);
	}
	
	/**
	 * Test para comprobar que el metodo 
	 * faltanporcolar() se realiza correctamente, de ser asi
	 * el metodo devolvera el mismo numero que la resta de la
	 * reserva inicial de la persona y los amigos que ya se han colado
	 */
	@Test
	public void testFaltanPorColar(){
		c.pedirLaVez(p1, 2);
		assertEquals((p1.getReserva() - c.consultaAmigosEnCola(p1)), c.faltanPorColar(p1));
	}
	
	/**
	 * Test para evitar que se pase un null
	 * como parametro al metodo faltan por colar
	 */
	@Test (expected = NullPointerException.class)
	public void testNoFaltanPorCOlarNull(){
		c.pedirLaVez(p1, 2);
		c.faltanPorColar(null);
	}
	
	/**
	 * Test para comprobar que amigosEnCola()
	 * funciona correctamente, de ser asi este debe devolver lo 
	 * mismo que la resta de la reserva inicial y los sitios aun
	 * disponibles
	 */
	@Test
	public void testAmigosEnCola(){
		c.pedirLaVez(p1, 2);
		assertEquals(c.consultaAmigosEnCola(p1), p1.getReserva()-c.faltanPorColar(p1));
	}
	
	/**
	 * Test para evitar que se pase null al metodo 
	 * amigosEnCola()
	 */
	@Test (expected = NullPointerException.class)
	public void testNoAmigosEnColaNull(){
		c.pedirLaVez(p1, 2);
		c.consultaAmigosEnCola(null);
	}
	
	/**
	 * Test para comprobar que siguiente funciona
	 * correctamente, si es asi este nos debe devolver la misma
	 * persona que el primer elemento del arrayList cola
	 */
	@Test
	public void testSiguiente(){
		c.pedirLaVez(p1, 0);
		assertEquals(c.siguiente(), c.getCola().get(0));
	}
	
	/**
	 * Test para controlar que se pida
	 * el siguiente en una cola vacia
	 */
	@Test (expected = IndexOutOfBoundsException.class)
	public void testSiguienteVacia(){
		c.atender();
	}
	
	/**
	 * Test para comprobar que el metodo atender funciona
	 * correctamente, de ser asi al finalizar la persona 
	 * atendida no debe estar en la cola
	 */
	@Test
	public void testAtender(){
		c.pedirLaVez(p1, 0);
		c.atender();
		assertFalse(c.getCola().contains(p1));
	}

	/**
	 * Test para comprobar que el getter de cola 
	 * devuelve el atributo cola 
	 */
	@Test
	public void testGetCola(){
		c.pedirLaVez(p1, 0);
		c.pedirLaVez(p2, 2);
		assertEquals(c.getCola(), c.cola);
	}
}
