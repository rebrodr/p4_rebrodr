package colaAmigos;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPersona {
	protected Persona p1;
	protected Persona p2;
	
	@Before
	public void setUp() {
		p1 = new Persona("ana");
		p2 = new Persona("maria");
	}
	
	@After
	public void tearDown() {
		p1 = null;
		p2 = null;
	}
	
	/**
	 * Test para comprobar que se ha creado bien una
	 * persona, de ser asi deberia tener un array de conocidos
	 * y de amigos vacio (tamaño 0) y el nombre pasado
	 * como parametro
	 */
	@Test
	public void testEsValida(){
		String nombre = "ana";
		Persona p = new Persona(nombre);
		assertEquals(p.getNombre(), nombre);
		assertEquals(p.getConocidos().size(), 0);
		assertEquals(p.getAmigos().size(), 0);
	}
	
	/**
	 * Test para evitar que se cree una persona sin nombre
	 */
	@Test
	public void testNoCreaSinNombre(){
		String nombre = null;
		Persona p = new Persona(nombre);
		assertNull(p.getNombre());
	}
	
	/**
	 * Test para comprobar si el metodo esamigo() funciona
	 * correctamente
	 */
	@Test
	public void testEsAmigo(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		assertTrue(p1.esAmigo(p2));
	}
	
	/**
	 * Test para asegurar que cuando una persona se hace
	 * amigo de otra, esta ultima se hace amiga del primero
	 */
	@Test
	public void testEsAmigoReciproco(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		assertTrue(p1.esAmigo(p2));
		assertTrue(p2.esAmigo(p1));
	}
	
	/**
	 * Test para evitar que se pase null como parametro
	 * a esAmigo()
	 */
	@Test (expected = NullPointerException.class)
	public void testNoEsAmigoNull(){
		p1.serAmigo(null);
	}
	
	/**
	 * Test para comprobar que el metodo conocer funciona
	 * correctamente, de ser asi al final la persona conocida
	 * debe estar en el arrayList de conocidos
	 */
	@Test
	public void testConocer(){
		p1.conocer(p2);
		assertTrue(p1.getConocidos().contains(p2));
	}
	
	/**
	 * Test para asegurar que cuando una persona conoce
	 * a otra, esta ultima tambien conoce al primero
	 */
	@Test
	public void testConocerReciproco(){
		p1.conocer(p2);
		assertTrue(p1.getConocidos().contains(p2));
		assertTrue(p2.getConocidos().contains(p1));
	}
	
	/**
	 * Test para evitar que se pase un null como parametro
	 * al metodo conocer()
	 */
	@Test (expected = NullPointerException.class)
	public void testNoConocerNull(){
		p1.conocer(null);
	}
	
	/**
	 * Test para evitar que se intente conocer a una persona
	 * que ya conoces
	 */
	@Test (expected=AssertionError.class)
	public void testNoConocerConocido(){
		p1.conocer(p2);
		p1.conocer(p2);
	}
	
	/**
	 * Test para comprobar que el metodo serAmigo se realiza
	 * de forma correcta, de ser asi al final el nuevo amigo 
	 * debe estar en el arrayList de amigos
	 */
	@Test
	public void testSerAmigo(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		assertTrue(p1.getAmigos().contains(p2));
	}
	
	/**
	 * Test para asegurar que cuando una persona se hace
	 * amiga de otra, esta ultima se hace amiga del primero
	 */
	@Test
	public void testSerAmigoReciproco(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		assertTrue(p1.getAmigos().contains(p2));
		assertTrue(p2.getAmigos().contains(p1));
	}
	
	/**
	 * Test para evitar que se pase un null como
	 * parametro al metodo 
	 */
	@Test (expected = NullPointerException.class)
	public void testNoSerAmigoNull(){
		p1.serAmigo(null);
	}
	
	/**
	 * Test para evitar que una persona se haga amiga otra
	 * de la cual ya es amiga
	 */
	@Test (expected=AssertionError.class)
	public void testNoSerAmigoDeAmigo(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		p1.serAmigo(p2);
	}
	/**
	 * Test para evitar que una persona intente trabar 
	 * amistad con alguien que no conoce
	 */
	@Test (expected= AssertionError.class)
	public void testNoSerAmigoDesconocido(){
		p1.serAmigo(p2);
		assertFalse(p1.getConocidos().contains(p2));
		assertFalse(p1.getAmigos().contains(p2));
	}
	
	/**
	 * Test para comprobar que el metodo dejarSerAmigo()
	 * funciona correctamente, de ser asi al finalizar se
	 * debe tener la persona en el arrayList de conocidos pero
	 * no en el de amigos
	 */
	@Test
	public void testDejarSerAmigo(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		p1.dejarSerAmigo(p2);
		assertFalse(p1.getAmigos().contains(p2));
		assertTrue(p1.getConocidos().contains(p2));
	}
	
	/**
	 * Test para asegurar que el metodo dejar de ser amigos
	 * es reciproco y si alguien deja de ser amigo de otro, este
	 * dejara de serlo del primero. Ambos seran solo conocidos.
	 */
	@Test
	public void testDejarSerAmigoReciproco(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		p1.dejarSerAmigo(p2);
		assertFalse(p1.getAmigos().contains(p2));
		assertFalse(p2.getAmigos().contains(p1));
		assertTrue(p1.getConocidos().contains(p2));
		assertTrue(p2.getConocidos().contains(p1));
	}
	
	/**
	 * Test para evitar que se trate de dejar de ser amigo
	 * de una persona de la cual no eres amigo
	 */
	@Test (expected=AssertionError.class)
	public void testNoDejarSerAmigoNoAmigo(){
		p1.dejarSerAmigo(p2);
	}
	
	/**
	 * Test para evitar que se pase null como parametro
	 * al metodo dejarSerAmigo()
	 */
	@Test (expected = NullPointerException.class)
	public void testNoDejarSerAmigoNull(){
		p1.dejarSerAmigo(null);
	}
	
	/**
	 * Test para asegurar que se cumple que si una persona
	 * se hace amiga de otra, deja de estar en el arrayList
	 * de conocidos y solo esta en el de amigos. No puede
	 * estar en ambos a la vez
	 */
	@Test (expected= AssertionError.class)
	public void testAmigoYConocido(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		assertTrue(p1.getConocidos().contains(p2));
		assertTrue(p1.getAmigos().contains(p2));
	}
	
	/**
	 * Test para comprobar que el getter de conocidos 
	 * devuelve el atributo conocidos 
	 */
	@Test
	public void testGetConocidos(){
		p1.conocer(p2);
		assertEquals(p1.getConocidos(), p1.conocidos);
	}

	/**
	 * Test para comprobar que el getter de amigos 
	 * devuelve el atributo amigos 
	 */
	@Test
	public void testGetAmigos(){
		p1.conocer(p2);
		p1.serAmigo(p2);
		assertEquals(p1.getAmigos(), p1.amigos);
	}
	
	/**
	 * Test para comprobar que el getter de nombre 
	 * devuelve el atributo nombre 
	 */
	@Test
	public void testGetNombre(){
		assertEquals(p1.getNombre(), p1.nombre);
	}
	
	/**
	 * Test para comprobar que si haces un set al
	 * atributo reserva, el getter te devolvera el
	 * set realizado
	 */
	@Test
	public void testSetGetReserva(){
		p1.setReserva(1);
		assertEquals(p1.getReserva(), p1.reserva);
	}
}
