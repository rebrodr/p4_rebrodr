package colaAmigos;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.easymock.Mock;

import static org.easymock.EasyMock.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MockTestColaAmigos { 
	protected ColaAmigos c;
	@Mock
	protected Persona p1;
	@Mock
	protected Persona p2;

	@Before
	public void setUp() {
		c = new ColaAmigos();
	}
	@After
	public void tearDown() {
		c = null;
	}
	/**
	 * Test en aislamiento para comprobar que se ha creado bien una
	 * cola, de ser asi deberia tener un array vacio (tamaño 0)
	 */
	@Test
	public void testEsValida(){
		assertEquals(c.getCola().size(), 0);
	}
	/**
	 * Test en aislamiento para comprobar que se pide la vez
	 * correctamente, al acabar se debe tener en la cola a la
	 * persona que pide vez
	 */
	@Test
	public void testPedirLaVez(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		assertTrue(c.getCola().contains(p1));
		verify(p1);
	}
	/**
	 * Test en aislamiento para impedir que null pueda 
	 * pedir la vez 
	 */
	@Test (expected = IllegalStateException.class)
	public void testPedirLaVezNoNull(){
		p1 = createMock(Persona.class);
		expect(p1).andReturn(null);
		replay(p1);
		c.pedirLaVez(p1, 1);
		verify(p1);
	}
	
	/**
	 * Test en aislamiento para impedir que una persona
	 * intente entrar en la cola ya estando (a traves del
	 * metodo pedir la vez)
	 */
	@Test (expected=AssertionError.class)
	public void testPedirLaVezRepetida(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		c.pedirLaVez(p1, 1);
	}

	/**
	 * Test en aislamiento para impedir que una persona reserve
	 * sitio para un numero negativo de personas
	 */
	@Test  (expected = AssertionError.class)
	public void testPedirLaVezNegativa(){
		p1 = createMock(Persona.class);
		p1.setReserva(-1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(-1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1,-1);
		verify(p1);
	}

	/**
	 * Test en aislamiento para limitar el numero de personas
	 * para las que se puede reservar a 10
	 */
	@Test (expected=AssertionError.class)
	public void testPedirLaVezMaxLim(){
		p1 = createMock(Persona.class);
		p1.setReserva(11);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(11).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 11);
		verify(p1);
	}

	/**
	 * Test en aislamiento para comprobar que se realiza
	 * correctamente el metodo colarse, de ser asi la persona que se 
	 * cuela debe estar en la cola y las personas que le quedan por colar
	 * al que reservo deben decrementarse en 1
	 */
	@Test
	public void testColarse(){
		ArrayList<Persona> amigos = new ArrayList<Persona>();
		p1 = createMock(Persona.class);
		p2 = createMock(Persona.class);
		amigos.add(p2);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		expect(p1.getAmigos()).andReturn(amigos).anyTimes();
		replay(p1);
		expect(p2.esAmigo(p1)).andReturn(true).anyTimes();
		replay(p2);
		c.pedirLaVez(p1, 1);
		c.colarse(p2);
		assertTrue(c.getCola().contains(p2));
		assertTrue(p2.esAmigo(p1));
		assertEquals(c.faltanPorColar(p1),p1.getReserva()-c.consultaAmigosEnCola(p1));
		verify(p1);
		verify(p2);
	}
	/**
	 * Test en aislamiento para evitar que alguien que ya
	 * esta en la cola se cuele de nuevo
	 */
	@Test (expected=AssertionError.class)
	public void testColarseRepetido(){
		ArrayList<Persona> amigos = new ArrayList<Persona>();
		p1 = createMock(Persona.class);
		p2 = createMock(Persona.class);
		amigos.add(p2);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		expect(p1.getAmigos()).andReturn(amigos).anyTimes();
		replay(p1);
		expect(p2.esAmigo(p1)).andReturn(true).anyTimes();
		replay(p2);
		c.pedirLaVez(p1, 1);
		c.colarse(p2);
		c.colarse(p2);
	}

	/**
	 * Test en aislamiento para evitar que alguien se cuele
	 * sin tener ningun amigo con sitios reservados
	 */
	@Test (expected=AssertionError.class)
	public void testNoColarseSinSitio(){
		ArrayList<Persona> amigos = new ArrayList<Persona>();
		p1 = createMock(Persona.class);
		p2 = createMock(Persona.class);
		amigos.add(p2);
		p1.setReserva(0);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(0).anyTimes();
		expect(p1.getAmigos()).andReturn(amigos).anyTimes();
		replay(p1);
		expect(p2.esAmigo(p1)).andReturn(true).anyTimes();
		replay(p2);
		c.pedirLaVez(p2, 0);
		c.colarse(p2);
		assertNotEquals(c.faltanPorColar(p2),0);
		verify(p1);
		verify(p2);
	}

	/**
	 * Test en aislamiento para evitar que se pase
	 * un null como parametro al metodo colarse
	 */
	@Test (expected=NullPointerException.class)
	public void testNoColarseNull(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		c.colarse(null);
		verify(p1);
	}

	/**
	 * Test en aislamiento para evitar que se cuele una
	 * persona sin amigos en la cola
	 */
	@Test (expected=AssertionError.class)
	public void testNoColarseSinAmigo(){
		ArrayList<Persona> amigos = new ArrayList<Persona>();
		p1 = createMock(Persona.class);
		p2 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		expect(p1.getAmigos()).andReturn(amigos).anyTimes();
		replay(p1);
		expect(p2.esAmigo(p1)).andReturn(false).anyTimes();
		replay(p2);
		c.pedirLaVez(p2, 1);
		c.colarse(p2);
		assertTrue(p2.esAmigo(p1));
		verify(p1);
		verify(p2);
	}

	/**
	 * Test en aislamiento para comprobar si consultar la reserva de
	 * una persona se realiza correctamente
	 */
	@Test
	public void testConsultaReserva(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		assertEquals(c.consultaReserva(p1), p1.getReserva());
		verify(p1);
	}

	/**
	 * Test en aislamiento para evitar que se pase un 
	 * null como parametro al metodo consultarReserva()
	 */
	@Test (expected = NullPointerException.class)
	public void testNoConsultaReservaNull(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1,1);
		c.consultaReserva(null);
		verify(p1);
	}

	/**
	 * Test en aislamiento para controlar que se consulte la
	 * la reserva de alguien que no esta en la cola
	 */
	@Test (expected = AssertionError.class)
	public void testNoConsultaReservaNoCola(){
		p1 = createMock(Persona.class);
		c.consultaReserva(p1);
	}

	/**
	 * Test en aislamiento para comprobar que el metodo 
	 * faltanporcolar() se realiza correctamente, de ser asi
	 * el metodo devolvera el mismo numero que la resta de la
	 * reserva inicial de la persona y los amigos que ya se han colado
	 */
	@Test
	public void testFaltanPorColar(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		assertEquals((p1.getReserva() - c.consultaAmigosEnCola(p1)), c.faltanPorColar(p1));
		verify(p1);
	}

	/**
	 * Test en aislamiento para evitar que se pase un null
	 * como parametro al metodo faltan por colar
	 */
	@Test (expected = NullPointerException.class)
	public void testNoFaltanPorColarNull(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		c.faltanPorColar(null);
		verify(p1);
	}

	/**
	 * Test en aislamiento para comprobar que amigosEnCla()
	 * funciona correctamente, de ser asi este debe devolver lo 
	 * mismo que la resta de la reserva inicial y los sitios aun
	 * disponibles
	 */
	@Test
	public void testAmigosEnCola(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		assertEquals(c.consultaAmigosEnCola(p1), p1.getReserva() - c.faltanPorColar(p1));
		verify(p1);
	}

	/**
	 * Test en aislamiento para evitar que se pase null
	 * al metodo amigosEnCOla()
	 */
	@Test (expected = NullPointerException.class)
	public void testNoAmigosEnColaNull(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		c.consultaAmigosEnCola(null);
		verify(p1);
	}

	/**
	 * Test en aislamiento para comprobar que siguiente funciona
	 * correctamente, si es asi este nos debe devolver la misma
	 * persona que el primer elemento del arrayList cola
	 */
	@Test
	public void testSiguiente(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		assertEquals(c.siguiente(), c.getCola().get(0));
		verify(p1);
	}

	/**
	 * Test en aislamiento para controlar que se pida
	 * el siguiente en una cola vacia
	 */
	@Test (expected = IndexOutOfBoundsException.class)
	public void testSiguienteVacia(){
		c.atender();
	}

	/**
	 * Test en aislamiento para comprobar que el metodo atender funciona
	 * correctamente, de ser asi al finalizar la persona 
	 * atendida no debe estar en la cola
	 */
	@Test
	public void testAtender(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		c.atender();
		assertFalse(c.getCola().contains(p1));
		verify(p1);
	}

	/**
	 * Test en aislamiento para comprobar que el getter de
	 * cola devuelve el atributo cola 
	 */
	@Test
	public void testGetCola(){
		p1 = createMock(Persona.class);
		p1.setReserva(1);
		expectLastCall().once();
		expect(p1.getReserva()).andReturn(1).anyTimes();
		replay(p1);
		c.pedirLaVez(p1, 1);
		assertEquals(c.getCola(), c.cola);
		verify(p1);
	}
}

